create table estado (
	id bigint identity(1,1),
	nome varchar(80) not null,

	primary key (id)
);

alter table cidade add constraint fk_cidade_estado
foreign key (estado_id) references estado (id);
