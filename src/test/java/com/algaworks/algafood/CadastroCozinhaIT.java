package com.algaworks.algafood;

import com.algaworks.algafood.domain.exception.CozinhaNaoEncontradaException;
import com.algaworks.algafood.domain.exception.EntidadeEmUsoException;
import com.algaworks.algafood.domain.service.CadastroCozinhaService;
import com.algaworks.algafood.util.ResourceUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.doThrow;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = AlgafoodApiApplication.class)
@AutoConfigureMockMvc
public class CadastroCozinhaIT {

    @MockBean
    private CadastroCozinhaService cadastroCozinha;

    @Autowired
    private MockMvc mvc;

    private String jsonCorretoCozinhaChinesa, jsonIncorretoCozinhaChinesa;

    private final String URL = "/cozinhas";

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);

        jsonCorretoCozinhaChinesa = ResourceUtils.getContentFromResource(
                "/json/correto/cozinha-chinesa.json");
        jsonIncorretoCozinhaChinesa = ResourceUtils.getContentFromResource(
                "/json/incorreto/cozinha-chinesa.json");
    }

    @Test
    public void deveAtribuirId_QuandoCadastrarCozinhaComDadosCorretos() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonCorretoCozinhaChinesa)
                                .characterEncoding("UTF-8")
                        )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test()
    public void deveFalhar_QuandoCadastrarCozinhaSemNome() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post(URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jsonIncorretoCozinhaChinesa))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test()
    public void deveFalhar_QuandoExcluirCozinhaEmUso() throws Exception {
        doThrow(new EntidadeEmUsoException("Entidade em uso"))
                .when(cadastroCozinha).excluir(1L);

        mvc.perform(MockMvcRequestBuilders
                        .delete("/cozinhas/{id}", 1L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test()
    public void deveFalhar_QuandoExcluirCozinhaInexistente() throws Exception {
        doThrow(new CozinhaNaoEncontradaException("Cozinha não encontrada"))
                .when(cadastroCozinha).excluir(100L);

        mvc.perform(MockMvcRequestBuilders
                        .delete("/cozinhas/{id}", 100L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test()
    public void deveFalhar_QuandoBuscaCozinhaInexistente() throws Exception {
        doThrow(new CozinhaNaoEncontradaException("Cozinha não encontrada"))
                .when(cadastroCozinha).buscarOuFalhar(100L);

        mvc.perform(MockMvcRequestBuilders
                        .get("/cozinhas/{id}", 100L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test()
    public void deveRetornaStatus200_QuandoBuscaCozinha() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .get("/cozinhas/{id}", 7L)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
