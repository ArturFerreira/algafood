create table cidade (id bigint identity not null, nome varchar(255) not null, estado_id bigint not null, primary key (id))
create table cozinha (id bigint identity not null, nome varchar(255) not null, primary key (id))
create table estado (id bigint identity not null, nome varchar(255) not null, primary key (id))
create table forma_pagamento (id bigint identity not null, descricao varchar(255) not null, primary key (id))
create table grupo (id bigint identity not null, nome varchar(255) not null, primary key (id))
create table grupo_permissao (grupo_id bigint not null, permissao_id bigint not null)
create table permissao (id bigint identity not null, descricao varchar(255) not null, nome varchar(255) not null, primary key (id))
create table produto (id bigint identity not null, ativo bit not null, descricao varchar(255) not null, nome varchar(255) not null, preco numeric(19,2) not null, restaurante_id bigint not null, primary key (id))
create table restaurante (id bigint identity not null, data_atualizacao datetime2 not null, data_cadastro datetime2 not null, endereco_bairro varchar(255), endereco_cep varchar(255), endereco_complemento varchar(255), endereco_logradouro varchar(255), endereco_numero varchar(255), nome varchar(255) not null, taxa_frete numeric(19,2) not null, cozinha_id bigint not null, endereco_cidade_id bigint, primary key (id))
create table restaurante_forma_pagamento (restaurante_id bigint not null, forma_pagamento_id bigint not null)
create table usuario (id bigint identity not null, data_cadastro datetime not null, email varchar(255) not null, nome varchar(255) not null, senha varchar(255) not null, primary key (id))
create table usuario_grupo (usuario_id bigint not null, grupo_id bigint not null)
alter table cidade add constraint FKkworrwk40xj58kevvh3evi500 foreign key (estado_id) references estado
alter table grupo_permissao add constraint FKh21kiw0y0hxg6birmdf2ef6vy foreign key (permissao_id) references permissao
alter table grupo_permissao add constraint FKta4si8vh3f4jo3bsslvkscc2m foreign key (grupo_id) references grupo
alter table produto add constraint FKb9jhjyghjcn25guim7q4pt8qx foreign key (restaurante_id) references restaurante
alter table restaurante add constraint FK76grk4roudh659skcgbnanthi foreign key (cozinha_id) references cozinha
alter table restaurante add constraint FKbc0tm7hnvc96d8e7e2ulb05yw foreign key (endereco_cidade_id) references cidade
alter table restaurante_forma_pagamento add constraint FK7aln770m80358y4olr03hyhh2 foreign key (forma_pagamento_id) references forma_pagamento
alter table restaurante_forma_pagamento add constraint FKa30vowfejemkw7whjvr8pryvj foreign key (restaurante_id) references restaurante
alter table usuario_grupo add constraint FKk30suuy31cq5u36m9am4om9ju foreign key (grupo_id) references grupo
alter table usuario_grupo add constraint FKdofo9es0esuiahyw2q467crxw foreign key (usuario_id) references usuario
create table cidade (id bigint identity not null, nome varchar(255) not null, estado_id bigint not null, primary key (id))
create table cozinha (id bigint identity not null, nome varchar(255) not null, primary key (id))
create table estado (id bigint identity not null, nome varchar(255) not null, primary key (id))
create table forma_pagamento (id bigint identity not null, descricao varchar(255) not null, primary key (id))
create table grupo (id bigint identity not null, nome varchar(255) not null, primary key (id))
create table grupo_permissao (grupo_id bigint not null, permissao_id bigint not null)
create table permissao (id bigint identity not null, descricao varchar(255) not null, nome varchar(255) not null, primary key (id))
create table produto (id bigint identity not null, ativo bit not null, descricao varchar(255) not null, nome varchar(255) not null, preco numeric(19,2) not null, restaurante_id bigint not null, primary key (id))
create table restaurante (id bigint identity not null, data_atualizacao datetime2 not null, data_cadastro datetime2 not null, endereco_bairro varchar(255), endereco_cep varchar(255), endereco_complemento varchar(255), endereco_logradouro varchar(255), endereco_numero varchar(255), nome varchar(255) not null, taxa_frete numeric(19,2) not null, cozinha_id bigint not null, endereco_cidade_id bigint, primary key (id))
create table restaurante_forma_pagamento (restaurante_id bigint not null, forma_pagamento_id bigint not null)
create table usuario (id bigint identity not null, data_cadastro datetime not null, email varchar(255) not null, nome varchar(255) not null, senha varchar(255) not null, primary key (id))
create table usuario_grupo (usuario_id bigint not null, grupo_id bigint not null)
alter table cidade add constraint FKkworrwk40xj58kevvh3evi500 foreign key (estado_id) references estado
alter table grupo_permissao add constraint FKh21kiw0y0hxg6birmdf2ef6vy foreign key (permissao_id) references permissao
alter table grupo_permissao add constraint FKta4si8vh3f4jo3bsslvkscc2m foreign key (grupo_id) references grupo
alter table produto add constraint FKb9jhjyghjcn25guim7q4pt8qx foreign key (restaurante_id) references restaurante
alter table restaurante add constraint FK76grk4roudh659skcgbnanthi foreign key (cozinha_id) references cozinha
alter table restaurante add constraint FKbc0tm7hnvc96d8e7e2ulb05yw foreign key (endereco_cidade_id) references cidade
alter table restaurante_forma_pagamento add constraint FK7aln770m80358y4olr03hyhh2 foreign key (forma_pagamento_id) references forma_pagamento
alter table restaurante_forma_pagamento add constraint FKa30vowfejemkw7whjvr8pryvj foreign key (restaurante_id) references restaurante
alter table usuario_grupo add constraint FKk30suuy31cq5u36m9am4om9ju foreign key (grupo_id) references grupo
alter table usuario_grupo add constraint FKdofo9es0esuiahyw2q467crxw foreign key (usuario_id) references usuario
insert into cozinha (nome) values ('Tailandesa')
insert into cozinha (nome) values ('Indiana')
insert into estado (nome) values ('Minas Gerais')
insert into estado (nome) values ('São Paulo')
insert into estado (nome) values ('Ceará')
insert into estado (nome) values ('Maranhão')
insert into cidade (nome, estado_id) values ('Uberlândia', 1)
insert into cidade (nome, estado_id) values ('Belo Horizonte', 1)
insert into cidade (nome, estado_id) values ('São Paulo', 2)
insert into cidade (nome, estado_id) values ('Campinas', 2)
insert into cidade (nome, estado_id) values ('Fortaleza', 3)
insert into cidade (nome, estado_id) values ('São Luís', 4)
insert into restaurante (nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao, endereco_cidade_id, endereco_cep, endereco_logradouro, endereco_numero, endereco_bairro) values ('Thai Gourmet', 10, 1, GETDATE(), GETDATE(), 1, '38400-999', 'Rua João Pinheiro', '1000', 'Centro')
insert into restaurante (nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) values ('Thai Delivery', 9.50, 1, GETDATE(), GETDATE())
insert into restaurante (nome, taxa_frete, cozinha_id, data_cadastro, data_atualizacao) values ('Tuk Tuk Comida Indiana', 15, 2, GETDATE(), GETDATE())
insert into forma_pagamento (descricao) values ('Cartão de crédito')
insert into forma_pagamento (descricao) values ('Cartão de débito')
insert into forma_pagamento (descricao) values ('Dinheiro')
insert into permissao (nome, descricao) values ('CONSULTAR_COZINHAS', 'Permite consultar cozinhas')
insert into permissao (nome, descricao) values ('EDITAR_COZINHAS', 'Permite editar cozinhas')
insert into restaurante_forma_pagamento (restaurante_id, forma_pagamento_id) values (1, 1), (1, 2), (1, 3), (2, 3), (3, 2), (3, 3)
