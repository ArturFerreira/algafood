# Algafood


<h4 align="center"> 
	🚧  Status do projeto 🚀 Em construção...  🚧
</h4>

### Features

- [x] Introdução
- [x] Spring e Injeção de Dependência
- [x] Introdução ao JPA e Hibernate
- [ ] Rest com Spring

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

- [Java]()
- [Spring](https://spring.io/)
- [Sql Server]()

### Autor
---

<a href="https://gitlab.com/ArturFerreira/algafood">

 <sub><b>Artur de Oliveira Ferreira</b></sub></a>


